AWS Cloud Formation is a service that helps you set up your AWS resources so that you can spend less time managing those resources and more time focusing on your applications that run in AWS. You create a template which is a JSON - or YAML-formatted text file that describes all the AWS resources that you want and Cloud Formation takes care of provisioning and configuring those resources for you. You don’t need to individually create and configure AWS resources and figure out what’s dependent on what; Cloud Formation handles that.

In this tutorial, I am going to walk you through a Cloud Formation template that is used to create a VPC and Auto Scaling group. The VPC will consist of two public and private subnets, and an Internet Gateway to allow internet access to the EC2 instances. The Auto Scaling group is configured with a minimum of two, three desired, and five max instances. The launch template for the Auto Scaling group consists of EC2 instances with user data to install Apache Web Server and a stress tool.

Although a template made up of ten sections, the Resources section is the only one required. For this project we will be using AWSTemplateFormatVersion, Descripton, Parameters and Resources.

A stack is a collection of AWS resources that you can manage as a single unit. All the resources in a stack are defined by the stack’s AWS CloudFormation template. A stack, for instance, can include all the resources required to run a web application, such as a web server, a database, and networking rules.

I have used extensively CloudFormation UserGuide to prepare my CloudFormation Template.
Pre-Requisites:

Access to AWS Console with an AWS Account (not root account).

Visual Studio Code Editor.
Steps to complete the project:
1. Build CloudFormation Template in Sections
2. Save the Template as a .yaml on your computer
3. Create the CloudFormation Stack — Upload your CloudFormation Template
4. Stress Test EC2 Instances
Build the CloudFormation Template in Sections:

Include all the items that you need in the template.
AWSTemplateFormatVersion:
Description:
Parameters:
Resources:

Use AWS CloudFormation built-in intrinsic functions in your Template to assign values to properties that are not available until runtime.

!Ref function(Syntax for the short form) returns the value of the specified parameter or resource.

Select function to select a single object from the list. In this case it is returning the first Availability Zone using the GetAZ function.

Sub function to update the AWS::Region pseudo parameter for the actual region the stack is created in. It uses the Base64 function to pass encoded data to EC2 instances.

substitutes variables in an input string with values that you specify. In your templates, you can use this function to construct commands or outputs that include values that aren't available until you create or update a stack.
